package uebung.rmi;
import java.io.BufferedReader;
import java.io.Console;
import java.io.InputStreamReader;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Client {

    private Client() {}

    public static void main(String[] args) {

    	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
//      String host = "141.72.135.189"; //vitali
//    	String host = "141.72.142.119"; //alex
    	String host = "localhost";
        try {
            Registry registry = LocateRegistry.getRegistry(host);
            Hello stub = (Hello) registry.lookup("Hello");
            while(true){
            	System.out.println("Type your Message:");
            	String s = br.readLine();
            	if (s.equals("exit")) {
					System.exit(0);
				}
            	stub.sayHello(s);
            }
            //stub.sayHello("VITALIIIIII");
            //String response = stub.sayHello();
            //System.out.println("response: " + response);
        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }
    }
}