package uebung.rmi;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Hello extends Remote {
    public void sayHello(String msg) throws RemoteException;
}